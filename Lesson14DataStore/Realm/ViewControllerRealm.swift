 
 
 import UIKit
 import RealmSwift
 
 
 class ToDoListItem: Object {
    @objc dynamic var item: String = ""
    @objc dynamic var date: Date = Date()
 }

class ViewControllerRealm: UIViewController {
  

    @IBOutlet weak var tableToDoList: UITableView!
    
 // add task
    @IBAction func addButton() {
        guard let vc = storyboard?.instantiateViewController(identifier: "enter") as? EntryViewController else {
            return
        }
        vc.completionHandler = {[weak self] in
            self?.refresh()
        }
            
      
        vc.title = "New Item"
    
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true )
    }
    
    func refresh() {
        data = realm.objects(ToDoListItem.self).map({ $0 })
        tableToDoList.reloadData()
    }
    
    private let realm = try! Realm()
    private var data = [ToDoListItem]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data = realm.objects(ToDoListItem.self).map({ $0 })
        tableToDoList.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableToDoList.delegate = self
        tableToDoList.dataSource = self
        
    }
    



}
 extension ViewControllerRealm: UITableViewDelegate, UITableViewDataSource {
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = data[indexPath.row].item
     //   cell.imageView?.image = UIImage(named: "checkmark")


        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = data[indexPath.row]
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "view") as? ViewViewController else {
            return
        }
        
        vc.item = item
        
        vc.deletionHandler = {[weak self] in
            self?.refresh()
        }
        vc.navigationItem.largeTitleDisplayMode = .never
        vc.title = item.item
    
        navigationController?.pushViewController(vc, animated: true)
     

    }
    
    
    
 }
