//
//  ViewController.swift
//  Lesson14DataStore
//
//  Created by Виктория Щербакова on 01.11.2020.
//  Copyright © 2020 Виктория Щербакова. All rights reserved.
//

import UIKit
var name: String {
    get {
        return UserDefaults.standard.string(forKey: "name") ?? ""
    }
    set {
        UserDefaults.standard.set(newValue, forKey:"name")
        UserDefaults.standard.synchronize()
    }
}

var surname: String {
    get {
        return UserDefaults.standard.string(forKey: "surname") ?? ""
    }
    set {
        UserDefaults.standard.set(newValue, forKey:"surname")
        UserDefaults.standard.synchronize()
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var surnameTextField: UITextField!
    
    @IBAction func saveButton(_ sender: UIButton) {
        name = nameTextField.text ?? ""
        surname = surnameTextField.text ?? ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.text! = name
        surnameTextField.text! = surname
    }


}

