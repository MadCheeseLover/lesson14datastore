
import UIKit
import CoreData

class TableViewControllerCoreData: UITableViewController {
    
    var tasks: [Tasks] = []

    
    @IBAction func addButton(_ sender: UIBarButtonItem) {
        
        let alertController = UIAlertController(title: "New task", message: "Add task", preferredStyle: .alert)
        
        let saveTask = UIAlertAction(title: "Save", style: .default) { action in
            
            let text = alertController.textFields?.first
            
            if let newTask = text?.text {
                self.saveTask(withTitle: newTask)
                self.tableView.reloadData()
            }
            
            
        }
        
        alertController.addTextField { _ in}
        
        let cancel = UIAlertAction(title: "Cancel", style: .default) { _ in}
        
        alertController.addAction(saveTask)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    // Функция, с пом. которой записываются данные в core data
    func saveTask(withTitle title: String) {
        
        // чтобы записать данные нужно сначала добраться до контекста
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        // добраться до сущности
        
        guard let tasksEntity = NSEntityDescription.entity(forEntityName: "Tasks", in: context) else {return}
        
        // добраться до объекта
        
        let tasksObject = Tasks(entity: tasksEntity, insertInto: context)
        tasksObject.title = title
        
 // записываем в дб
        do {
            try context.save()
            tasks.insert(tasksObject, at: 0)
            
            
        } catch let error as NSError{
            print(error.localizedDescription)
        }
        
        
    }
    
    
//    @IBAction func deleteButton(_ sender: UIBarButtonItem) {
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//        
//        let fetchRequest: NSFetchRequest<Tasks> = Tasks.fetchRequest()
//        
//        if let tasks = try? context.fetch(fetchRequest) {
//            
//          
//            for task in tasks {
//                context.delete(task)
//            }
//        }
//        
//        do {
//            try context.save()
//            
//        } catch let error as NSError{
//            print(error.localizedDescription)
//        }
//        
//        tableView.reloadData()
//        
//    }
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
// запрос, чтобы получить массив объектов из Core Data
        let fetchRequest: NSFetchRequest<Tasks> = Tasks.fetchRequest()
        
        do {
            tasks = try context.fetch(fetchRequest)
           
            
        } catch let error as NSError{
            print(error.localizedDescription)
            
        }
        
        
    }
        
        override func viewDidLoad() {
        super.viewDidLoad()
            
       //  tableView.reloadData()


    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tasks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        
        let task = tasks[indexPath.row]
        cell.textLabel?.text = task.title
        return cell
        
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let appDelegate2 = UIApplication.shared.delegate as? AppDelegate else {return}
        let context2 = appDelegate2.persistentContainer.viewContext
        let deleteTask = tasks[indexPath.row]
        context2.delete(deleteTask)
        
        do {
            try context2.save()
            
            tasks.remove(at: indexPath.row)
            tableView.reloadData()
            
        }
        catch let error as NSError{
            print("Failed", error)
        }
        
    }
    

}
